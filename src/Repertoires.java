import java.util.ArrayList;
 

  public class Repertoires extends Obj{
 
	private ArrayList<Obj> CR;	
	private boolean feuille;
	
	public Repertoires(String nom){
		super( nom);
		this.CR=new ArrayList<Obj>();
		feuille=true;
	
	}
	
	public ArrayList<Obj> getCR (){
		return CR;
	}
	
	public void addObj(Obj obj) {
		if (obj.isRep()==false)
			this.CR.add(obj);
		
		else
		if (this==obj) 
			System.out.println("repertoire ne peut pas contenir lui m�me"); 
		else 
			if (estDescendant((Repertoires)obj))
			System.out.println("repertoire ne peut pas �tre ajout� comme descendant de lui m�me"); 
			
			else
				if (isFeuille()) {
					this.notFeuille();
					this.CR.add(obj);
				}
					

	}
	
	public boolean estDescendant (Repertoires rep){
		boolean res =false;
		if (rep.isFeuille()==false) 
			for (Obj o: rep.getCR()){
				if (o.isRep())
					if (o==this) return true;
					else 
						res= estDescendant((Repertoires)o);
			}
		return false;
	}
	
	public int getTaille() {
		int res=0;
		for (Obj o: CR){
			res+=o.getTaille();
		}
		return res;
	}
	
	public boolean isRep () {
		return true;
	}
	
	public boolean isFeuille () {
		return feuille;
	}
	
	public void notFeuille (){
		feuille =false;
	}
	
}
