
public abstract class Obj{

	protected String nom;
	
	public Obj(String nom){
		this.nom = nom;
	}

	public String getNom () {
		return this.nom;
	}
	
	public abstract int getTaille();
	public abstract boolean isRep();

}